// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BfGameGameMode.generated.h"

UCLASS(minimalapi)
class ABfGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABfGameGameMode();
};



