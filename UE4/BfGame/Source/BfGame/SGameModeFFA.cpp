// Fill out your copyright notice in the Description page of Project Settings.

#include "SGameModeFFA.h"
#include "Async.h"
#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"

ASGameModeFFA::ASGameModeFFA()
{
	// Enable Tick function
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
}

void ASGameModeFFA::StartPlay()
{
	Super::StartPlay();

	this->OnActorKilled.AddDynamic(this, &ASGameModeFFA::HandleOnActorKilled);
}

void ASGameModeFFA::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// Handle respawn queue
	for (auto& RespawnPair : RespawnQueue) {
		RespawnPair.second -= DeltaSeconds;
		if (RespawnPair.second <= 0.0f) {
			RespawnPlayer(RespawnPair.first);
			RespawnQueue.erase(RespawnPair.first);
		}
	}
}

void ASGameModeFFA::HandleOnActorKilled(AActor* VictimActor, AController* VictimController, AActor* KillerActor, AController* KillerController)
{
	UE_LOG(LogTemp, Log, TEXT("Actor died"));
	RespawnQueue[VictimController] = 3.0f;
}

void ASGameModeFFA::RespawnPlayer(AController* VictimController)
{
	APlayerController* APC = Cast<APlayerController>(VictimController);
	if (APC && APC->GetPawn() == nullptr) {
		// Get random spawn point
		TArray<AActor*> SpawnPoints;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), SpawnPoints);
		AActor* SpawnPoint = SpawnPoints[rand() % SpawnPoints.Num()];

		UE_LOG(LogTemp, Log, TEXT("Respawning actor"));
		RestartPlayerAtPlayerStart(APC, SpawnPoint);
	}
}
