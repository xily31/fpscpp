// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <map>

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SGameModeFFA.generated.h"

// On actor killed event
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnActorKilled, AActor*, VictimActor, AController*, VictimController, AActor*, KillerActor, AController*, KillerController); // Killed actor, Killer actor

/**
 * 
 */
UCLASS()
class BFGAME_API ASGameModeFFA : public AGameModeBase
{
	GENERATED_BODY()
	

protected:

public:
	ASGameModeFFA();

	virtual void StartPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION()
	void HandleOnActorKilled(AActor* VictimActor, AController* VictimController, AActor* KillerActor, AController* KillerController);

	UPROPERTY(BlueprintAssignable, Category = "GameMode")
		FOnActorKilled OnActorKilled;

	UFUNCTION()
		void RespawnPlayer(AController* VictimController);

	std::map<AController*, float> RespawnQueue;
};
