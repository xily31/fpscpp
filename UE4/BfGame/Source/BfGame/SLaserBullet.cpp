// Fill out your copyright notice in the Description page of Project Settings.

#include "SLaserBullet.h"

#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASLaserBullet::ASLaserBullet()
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(10.0f);
	CollisionComp->SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &ASLaserBullet::OnHit);	// set up a notification for when this component hits something blocking
	CollisionComp->SetCanEverAffectNavigation(false);
	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;
	// Set as root component
	RootComponent = CollisionComp;

	MovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MovementComp"));
	MovementComp->UpdatedComponent = CollisionComp;
	MovementComp->InitialSpeed = 4500.f;
	MovementComp->MaxSpeed = 4500.f;
	MovementComp->bShouldBounce = true;
	

	// Die after 1 seconds by default
	InitialLifeSpan = 1.0f;

	// Enable replication (maybe need to tweak this shite someday, opti)
	SetReplicates(true);
	SetReplicateMovement(true);

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComp->CanCharacterStepUpOn = ECB_No;
	MeshComp->SetCanEverAffectNavigation(false);
	MeshComp->SetupAttachment(CollisionComp);

	OnDestroyed.AddDynamic(this, &ASLaserBullet::OnDestroyedEvent);

	DamageRadius = 250.0f;
}

// Called when the game starts or when spawned
void ASLaserBullet::BeginPlay()
{
	Super::BeginPlay();
}

void ASLaserBullet::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (Role >= ROLE_Authority) {
		if (OtherActor) {
			HitActor = OtherActor;
		}
		Destroy();
	}
	
}

void ASLaserBullet::OnDestroyedEvent(AActor* DestroyedActor)
{
	//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "Projectile exploded");
	if (ExplosionEffect) {
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation());
	}

	if (DamageType) {
		TArray<AActor*> IgnoredActors;
		UGameplayStatics::ApplyRadialDamage(GetWorld(), 20.0f, GetActorLocation(), DamageRadius, DamageType, IgnoredActors, GetOwner(), nullptr, false, ECC_EngineTraceChannel1);
		FHitResult HitRes;
		AController* InstigatorController = nullptr;
		if (Instigator && Instigator->IsPlayerControlled()) {
			InstigatorController = Instigator->GetController();
		}
		if (HitActor) {
			UGameplayStatics::ApplyPointDamage(HitActor, 20.0f, FVector::ZeroVector, HitRes, InstigatorController, Instigator, DamageType);
		}
		//DrawDebugSphere(GetWorld(), GetActorLocation(), DamageRadius, 16, FColor::Red, false, 3.0f);
	}
}