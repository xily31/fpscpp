// Fill out your copyright notice in the Description page of Project Settings.

#include "SLasergun.h"
#include "SLaserBullet.h"
#include "Components/SkeletalMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Components/SkeletalMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Net/UnrealNetwork.h"

void ASLasergun::Fire()
{
	// TODO : add some local feedback like muzzle flash, recoil

	// Projectile spawning logic is handled by server only
	// Projectiles are replicated, which is how they will be display client-side
		// Trace the world, from pawn eyes to cross hair location

	AActor* MyOwner = GetOwner();

	if (MyOwner) {
		NoticeFireToOwner();
		FVector EyeLocation;
		FRotator EyeRotation;
		MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		FVector ShotDirection = EyeRotation.Vector();
		FVector TraceEnd = EyeLocation + (ShotDirection * 10000);

		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(MyOwner);
		QueryParams.AddIgnoredActor(this);
		QueryParams.bTraceComplex = true; // Check on every triangle of hit object

		FHitResult Hit;

		// Check if we're aiming at something
		GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, ECC_Visibility, QueryParams);

		if (MuzzleEffect) {
			UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, MeshComp, MuzzleSocketName);
		}

		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);

		//Set Spawn Collision Handling Override
		FActorSpawnParameters ActorSpawnParams;
		ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		APawn* MyPawnOwner = Cast<APawn>(MyOwner);
		if (MyPawnOwner) {
			ActorSpawnParams.Instigator = MyPawnOwner;
		}

		// Spawn the projectile at the muzzle location
		//GetWorld()->SpawnActor<ASLaserBullet>(ProjectileClass, MuzzleLocation, ShotDirection.Rotation(), ActorSpawnParams);
		ServerFireProjectile(MuzzleLocation, ShotDirection.Rotation(), MyOwner);
	}



	//ServerFireProjectile();
}

bool ASLasergun::ServerFireProjectile_Validate(FVector Start, FRotator BulletDirection, AActor* BulletInstigator)
{
	return true;
}

void ASLasergun::ServerFireProjectile_Implementation(FVector Start, FRotator BulletDirection, AActor* BulletInstigator)
{
	APawn* PawnInstigator = Cast<APawn>(BulletInstigator);
	if (PawnInstigator)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.Instigator = PawnInstigator;
		GetWorld()->SpawnActor<ASLaserBullet>(ProjectileClass, Start, BulletDirection, SpawnParams);
	}
}

void ASLasergun::ServerFire_Implementation()
{

}

bool ASLasergun::ServerFire_Validate()
{
	return true;
}
