// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/World.h"
#include "CoreMinimal.h"
#include "SWeapon.h"
#include "SLasergun.generated.h"

class ASLaserBullet;

/**
 * 
 */
UCLASS()
class BFGAME_API ASLasergun : public ASWeapon
{
	GENERATED_BODY()
	
protected:
	virtual void Fire() override;

	UPROPERTY(EditDefaultsOnly, Category = "Projectile")
	TSubclassOf<ASLaserBullet> ProjectileClass;

	virtual bool ServerFire_Validate() override;

	virtual void ServerFire_Implementation() override;

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void ServerFireProjectile(FVector Start, FRotator BulletDirection, AActor* BulletInstigator);
};
