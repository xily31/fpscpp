#pragma once

#include "WeaponClient.generated.h"

UINTERFACE(Blueprintable)
class UWeaponClientInterface : public UInterface
{
	GENERATED_BODY()
};

class IWeaponClientInterface
{
	GENERATED_BODY()

public:
	/** React to a shot fired from the weapon. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Fire")
	bool ReactToFire();
};